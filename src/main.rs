use sysinfo::{ProcessExt, SystemExt};
use std::{thread, time};
use std::collections::HashSet;
use libnotify;

struct ProcData {
    sys: sysinfo::System,
    seen_procs: HashSet<i32>,
}

fn match_repo(name: &str, cmd: &[std::string::String]) -> bool {
    name == "python"
        && cmd.contains(&"--wrapper-path=/usr/bin/repo".to_string())
}

fn match_android_make(name: &str, cmd: &[std::string::String]) -> bool {
/* /worker/v8.1/out/soong_ui --make-mode -j6 android_disk_vdi */
    (name == "soong_ui" || name == "make")
        && (cmd.contains(&"--make-mode".to_string())
        || cmd.contains(&"make".to_string()))
}

fn check_procs(proc_data: &mut ProcData) {
    let monitored = [
        match_repo,
        match_android_make,
    ];

    let procs = proc_data.sys.get_processes();

    /* Check known and current PIDs intersection. */
    let curr_procs: HashSet<_> = procs.keys().cloned().collect();
    let finished_procs = proc_data.seen_procs.difference(&curr_procs).cloned().collect::<Vec<_>>();

    for pid in finished_procs {
        println!("process {} has finished :)", pid);
        notify("Process finished!");
        proc_data.seen_procs.remove(&pid);
    }

    /* Update proc list */
    for (&pid, proc) in procs {
        if proc_data.seen_procs.contains(&pid) {
            continue;
        }

        for matcher in &monitored {
            if matcher(proc.name(), proc.cmd()) {
                println!("{} is now running as pid {}", proc.name(), pid);
                proc_data.seen_procs.insert(pid);
            }
        }
    }
}

fn notify(content: &str) {
    // Create a new notification (doesn't show it yet)
    let n = libnotify::Notification::new("Process manager",
                                         Some(content),
                                         None);
    // Show the notification
    n.show().unwrap();
}

fn main() {
    print!("\x1B[2J");
    println!("sysmgr started");

    libnotify::init("sysmgr").unwrap();

    let loop_wait = time::Duration::from_millis(2000);

    let mut proc_data = ProcData {
        sys: sysinfo::System::new_all(),
        seen_procs: HashSet::new()
    };

    loop {
        proc_data.sys.refresh_processes();
        check_procs(&mut proc_data);

        thread::sleep(loop_wait);
    }

    //libnotify::uninit();
}
